package com.lp.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Redis Geo 工具类
 * 
 * @author zhanglei5
 *
 */
@Component
public class RedisUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisUtil.class);

    private static final int BATCH_SIZE = 5000;

    @Resource(name = "redisPool")
    private JedisPool redisPool;

    /**
     * key是否存在
     * 
     * @param key
     * @return
     */
    public boolean isKeyExist(String key) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.exists(key);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    /**
     * 设置key的过期时间
     * 
     * @param key
     * @param seconds
     */
    public Long setKeyExpire(String key, int seconds) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.expire(key, seconds);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public String get(String key) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.get(key);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public String set(String key, String value) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.set(key, value);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public String setex(String key, String value, int seconds) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.setex(key, seconds, value);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public boolean setKeyUnlockAndExpire(String key, Integer second) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            long lockResult = jedis.setnx(key, key);
            if (1 != lockResult) {
                return false;
            }
            if (second != null) {
                jedis.expire(key, second);
            }
            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public Long setIfNotExist(String key, String value) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.setnx(key, value);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public boolean isMapKeyExist(String key, String mapKey) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.hexists(key, mapKey);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public Long setMap(String key, String mapKey, String mapVal) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.hset(key, mapKey, mapVal);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public String setMapAll(String key, Map<String, String> map) {
        if (MapUtils.isEmpty(map)) {
            return "";
        }
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.hmset(key, map);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public String batchSetMapAll(String key, Map<String, String> map) {
        if (MapUtils.isEmpty(map)) {
            return "";
        }
        Jedis jedis = null;
        Pipeline pipeline = null;
        try {
            jedis = redisPool.getResource();
            pipeline = jedis.pipelined();
            Set<String> mapKeySet = map.keySet();
            int index = 0;
            for (String mapKey : mapKeySet) {
                pipeline.hset(key, mapKey, map.get(mapKey));
                if (++index % BATCH_SIZE == 0) {
                    pipeline.sync();
                }
            }
            pipeline.sync();
            return "OK";
        } catch (Exception e) {
            throw e;
        } finally {
            closePipeline(jedis, pipeline);
        }
    }

    public String getMap(String key, String mapKey) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.hget(key, mapKey);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public Map<String, String> batchGetMap(String key, String... mapKeys) throws UnsupportedEncodingException {
        if (mapKeys == null || mapKeys.length < 1) {
            return new HashMap<>();
        }
        Jedis jedis = null;
        Pipeline pipeline = null;
        try {
            jedis = redisPool.getResource();
            pipeline = jedis.pipelined();
            Map<String, String> result = new HashMap<>();
            int index = 0;
            Map<byte[], Response<byte[]>> newMap = new HashMap<>();
            for (String mapKey : mapKeys) {
                newMap.put(mapKey.getBytes("UTF-8"), pipeline.hget(key.getBytes("UTF-8"), mapKey.getBytes("UTF-8")));
                if (++index % BATCH_SIZE == 0) {
                    pipeline.sync();
                }
            }
            pipeline.sync();
            for (Map.Entry<byte[], Response<byte[]>> entry : newMap.entrySet()) {
                if (entry.getKey() == null) {
                    continue;
                }
                Response<byte[]> sResponse = entry.getValue();
                if (sResponse == null) {
                    continue;
                }
                if (sResponse.get() == null) {
                    continue;
                }
                result.put(new String(entry.getKey()), new String(sResponse.get()).toString());
            }
            return result;
        } catch (Exception e) {
            throw e;
        } finally {
            closePipeline(jedis, pipeline);
        }
    }

    public Map<String, String> getMapAll(String key) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.hgetAll(key);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public Map<String, String> batchGetMapAll(String key) throws UnsupportedEncodingException {
        Jedis jedis = null;
        Pipeline pipeline = null;
        try {
            jedis = redisPool.getResource();
            pipeline = jedis.pipelined();
            Map<String, String> result = new HashMap<>();
            Set<String> mapKeys = jedis.hkeys(key);
            if (CollectionUtils.isEmpty(mapKeys)) {
                return result;
            }
            int index = 0;
            Map<byte[], Response<byte[]>> newMap = new HashMap<>();
            for (String mapKey : mapKeys) {
                newMap.put(mapKey.getBytes(), pipeline.hget(key.getBytes("UTF-8"), mapKey.getBytes("UTF-8")));
                if (++index % BATCH_SIZE == 0) {
                    pipeline.sync();
                }
            }
            pipeline.sync();
            for (Map.Entry<byte[], Response<byte[]>> entry : newMap.entrySet()) {
                if (entry.getKey() == null) {
                    continue;
                }
                Response<byte[]> sResponse = entry.getValue();
                if (sResponse == null) {
                    continue;
                }
                if (sResponse.get() == null) {
                    continue;
                }
                result.put(new String(entry.getKey(), "UTF-8"), new String(sResponse.get(), "UTF-8").toString());
            }
            return result;
        } catch (Exception e) {
            throw e;
        } finally {
            closePipeline(jedis, pipeline);
        }
    }

    public Long deleteMapKeys(String key, String... mapKeys) {
        if (mapKeys == null || mapKeys.length < 1) {
            return 0L;
        }
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.hdel(key, mapKeys);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public Long deleteKey(String key) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.del(key);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public Long deleteKeyBatch(List<String> keyList) {
        if (CollectionUtils.isEmpty(keyList)) {
            return 0L;
        }
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.del((String[]) keyList.toArray());
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public Long deleteZSetKeys(String key, String... zsetKeys) {
        if (zsetKeys == null || zsetKeys.length < 1) {
            return 0L;
        }
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.zrem(key, zsetKeys);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public Long listHeadAdd(String key, String... vals) {
        if (vals == null || vals.length < 1) {
            return 0L;
        }
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.lpush(key, vals);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public Long listTailAdd(String key, String... vals) {
        if (vals == null || vals.length < 1) {
            return 0L;
        }
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.rpush(key, vals);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public List<String> listSub(String key, long begin, long end) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.lrange(key, begin, end);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public Long listSize(String key) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.llen(key);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    public String renameKey(String oldKey, String newKey) {
        Jedis jedis = null;
        try {
            jedis = redisPool.getResource();
            return jedis.rename(oldKey, newKey);
        } catch (Exception e) {
            throw e;
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
    }

    /**
     * 从地点列表中移除当前地点
     * 
     * @param dataList
     * @param currPosition
     * @return
     */
    private List<List<String>> removeCurrPos(List<List<String>> dataList, String currPosition, String empPrefix) {
        Iterator<List<String>> iterator = dataList.iterator();
        while (iterator.hasNext()) {
            List<String> row = (List<String>) iterator.next();
            String driverIdStr = row.get(0);
            if (StringUtils.isBlank(driverIdStr)) {
                iterator.remove();
                continue;
            }
            if (currPosition.equals(driverIdStr) && "0.0000".equals(row.get(1))) {
                iterator.remove();
                continue;
            }
            if (driverIdStr.startsWith(empPrefix)) {
                iterator.remove();
                continue;
            }
        }
        return dataList;
    }

    private void closePipeline(Jedis jedis, Pipeline pipeline) {
        try {
            if (null != pipeline) {
                pipeline.close();
            }
            if (null != jedis) {
                jedis.close();
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
