package com.lp.lock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.util.Pool;

import java.net.SocketTimeoutException;

/**
 * 实现Jedis对象的封装，实现操作
 */
public class RedisTemplate {

    private static Logger LOGGER = LoggerFactory.getLogger(RedisTemplate.class);

    private static int RETRY_CONNECTION_MAX_TIMES = 5;
    /**
     * 封装一个pool池用于jedis对象的管理
     */
    private Pool<Jedis> jedisPool;

    public RedisTemplate(Pool<Jedis> jedisPool) {
        this.jedisPool = jedisPool;
    }

    /**
     * 通过观察者模式实现事件的回调处理（使用范型）
     *
     * @param jedisAction 需要执行的方法接口
     * @param <T> 执行发放的返回类型
     * @return T
     */
    public <T> T execute(RedisAction<T> jedisAction) {
        T result = null;
        Jedis jedis = null;
        try {
            jedis = getResource();
            result = jedisAction.action(jedis);
        } catch (JedisConnectionException e) {
            LOGGER.error("Redis connection lost.", e);
        } finally {
            // 释放jedis资源
            closeResource(jedis);
            return result;
        }
    }

    /**
     * 通过观察者模式实现事件的回调处理
     */
    public void execute(RedisNoResultAction jedisAction) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedisAction.actionNoResult(jedis);
        } catch (JedisConnectionException e) {
            LOGGER.error("Redis connection lost.", e);
        } finally {
            closeResource(jedis);
        }
    }

    /**
     * 将jedis资源恢复到pool中
     *
     * @param jedis jedis资源
     */
    protected void closeResource(Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }
    }

    /**
     * 获取jedisPool对象.
     */
    public Pool<Jedis> getJedisPool() {
        return jedisPool;
    }

    private Jedis getResource() {
        int timeoutCount = 0;
        // 如果是网络超时则多试几次
        while (true) {
            try {
                return jedisPool.getResource();
            } catch (Exception e) {
                // 底层原因是SocketTimeoutException，不过redis已经捕捉且抛出JedisConnectionException，不继承于前者
                if (e instanceof JedisConnectionException || e instanceof SocketTimeoutException) {
                    timeoutCount++;
                    LOGGER.warn("getJedis timeoutCount={}", timeoutCount);
                    if (timeoutCount > RETRY_CONNECTION_MAX_TIMES) {
                        throw e;
                    }
                } else {
                    LOGGER.warn("jedisInfo。NumActive=" + jedisPool.getNumActive() + ", NumIdle="
                            + jedisPool.getNumIdle() + ", NumWaiters=" + jedisPool.getNumWaiters() + ", isClosed="
                            + jedisPool.isClosed());
                    LOGGER.error("getJedis error", e);
                    throw e;
                }
            }
        }
    }
}
