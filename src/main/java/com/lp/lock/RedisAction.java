package com.lp.lock;

import redis.clients.jedis.Jedis;

/**
 * redis操作
 *
 * @author LiuPeng
 * @date 2017/8/17 18:32
 */

public interface RedisAction<T> {

    <T> T action(Jedis jedis);
}
