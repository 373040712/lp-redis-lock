package com.lp.lock;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;

import javax.annotation.Resource;

/**
 * 通过redis实现对于分布式的环境下的并发管理（通过redis锁实现并发处理） 通过注解实现
 */
@Aspect
@Component
public class RedisLockOperation {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisLockOperation.class);

    @Resource(name = "redisPool")
    private JedisPool redisPool;

    /**
     * 创建一个redis锁
     * 
     * @param proceed 实际处理对象
     * @param redisLock RedisLock注解对象
     */
    @Around("@annotation(redisLock) && execution (* *(..))")
    public void acquireLock(final ProceedingJoinPoint proceed, final RedisLock redisLock) {
        RedisTemplate redisTemplate = new RedisTemplate(redisPool);
        RedisLockModel redisLockModel = RedisLockExecute.acquireLock(redisTemplate, redisLock.redisKeys(),
                redisLock.maxWait(), redisLock.expiredTime());
        try {
            if (RedisLockExecute.ACQUIRE_RESULT(redisLockModel)) {
                proceed.proceed();
            } else {
                LOGGER.debug("acquire lock is failed!");
            }
        } catch (Throwable throwable) {
            LOGGER.error("proceed={} is failed!", proceed.toString());
            throwable.printStackTrace();
        } finally {
            RedisLockExecute.releaseLock(redisTemplate, redisLockModel);
        }
    }
}
