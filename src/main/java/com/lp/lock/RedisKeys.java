package com.lp.lock;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author LiuPeng
 * @date 2017/8/17 14:30
 */
@Getter
@AllArgsConstructor
public enum RedisKeys {

    DEFAULT_KEY(0, "default_key");

    private final int code;

    private final String name;

}
