package com.lp.lock;

import redis.clients.jedis.Jedis;

/**
 * @author LiuPeng
 * @date 2017/8/24 12:50
 */

public interface RedisNoResultAction {

    void actionNoResult(Jedis jedis);
}
