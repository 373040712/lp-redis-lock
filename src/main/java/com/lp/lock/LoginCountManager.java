package com.lp.lock;

import org.springframework.stereotype.Component;

/**
 * @author LP
 * @date 2017/11/30 15:00
 */
@Component
public class LoginCountManager {

    @RedisLock(redisKeys = RedisKeys.DEFAULT_KEY, maxWait = 5000, expiredTime = 60000)
    public void doLoginCount(LoginInfo loginInfo) {
        loginInfo.setIndex(loginInfo.getIndex() + 1);
        System.out.println(loginInfo.getIndex());
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
