package com.lp.lock;

import lombok.Getter;
import lombok.Setter;

/**
 * @author LiuPeng
 * @date 2017/8/17 14:25
 */

public class RedisLockModel {
    /**
     * 锁id
     */
    @Setter
    @Getter
    private String lockId;

    /**
     * 锁name
     */
    @Getter
    private String lockName;

    public RedisLockModel(String lockName){
        this.lockName = lockName;
    }
}
