package com.lp.lock;

import lombok.Data;

/**
 * @author LP
 * @date 2017/11/30 15:18
 */
@Data
public class LoginInfo {
    private int index;
}
