package com.lp.lock;

import com.lp.test.BaseTest;
import org.junit.Test;
import org.springframework.core.task.TaskExecutor;

import javax.annotation.Resource;

/**
 * 登录计数统计
 *
 * @author LP
 * @date 2017/11/30 14:28
 */

public class LoginCountTest extends BaseTest {

    @Resource
    private LoginCountManager loginCountManager;

    @Resource
    private TaskExecutor taskExecutor;

    @Test
    public void testLoginCount() throws InterruptedException {
        LoginInfo loginInfo = new LoginInfo();
        for (int i = 0; i < 1000; i++) {
            taskExecutor.execute(() -> loginCountManager.doLoginCount(loginInfo));
            taskExecutor.execute(() -> loginCountManager.doLoginCount(loginInfo));
            taskExecutor.execute(() -> loginCountManager.doLoginCount(loginInfo));
            taskExecutor.execute(() -> loginCountManager.doLoginCount(loginInfo));
            taskExecutor.execute(() -> loginCountManager.doLoginCount(loginInfo));
        }
        Thread.sleep(300000);
        System.out.println("result: " + loginInfo.getIndex());
    }
}
